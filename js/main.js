document.addEventListener('DOMContentLoaded', function () {
    // Variables
    const PRODUCTOS = [
        {
            id: 1,
            nombre: 'Manzanas',
            imagen: 'img/manzanas.jpg',
            precio: 2.3
        },
        {
            id: 2,
            nombre: 'Fresas',
            imagen: 'img/fresas.jpg',
            precio: 1.5
        },
        {
            id: 3,
            nombre: 'Kiwis',
            imagen: 'img/kiwis.jpg',
            precio: 4
        },
        {
            id: 4,
            nombre: 'Limones',
            imagen: 'img/limones.jpg',
            precio: 3.2
        },
        {
            id: 5,
            nombre: 'Moras',
            imagen: 'img/moras.jpg',
            precio: 1.2
        },
        {
            id: 6,
            nombre: 'Naranjas',
            imagen: 'img/naranjas.jpg',
            precio: 0.78
        },
        {
            id: 7,
            nombre: 'Peras',
            imagen: 'img/peras.jpg',
            precio: 2.7
        },
        {
            id: 8,
            nombre: 'Piñas',
            imagen: 'img/pinyas.jpg',
            precio: 5
        },
        {
            id: 9,
            nombre: 'Platanos',
            imagen: 'img/platanos.jpg',
            precio: 1.4
        },
        {
            id: 10,
            nombre: 'Sandias',
            imagen: 'img/sandias.jpg',
            precio: 5.6
        },
        {
            id: 11,
            nombre: 'Uvas',
            imagen: 'img/uvas.jpg',
            precio: 1.4
        }
    ];
    let fichaProducto = document.querySelector('#fichaProducto');
    let inputBuscar = document.querySelector('#input_buscar');
    // Contante de productos por pagina
    const MAX_PRODUCTS_PAGE = 6;
    let pag = 1;
    let botonAnterior = document.querySelector('#boton_anterior');
    let botonSiguiente = document.querySelector('#boton_siguiente');

    // Funciones
    let addProducto = (id, nombre, imagen, precio) => {
        // Generamos los elementos
        /// Nodo column
        let column = document.createElement('div');
        column.classList.add('column', 'is-4');
        /// Nodo card
        let card = document.createElement('div');
        card.classList.add('card');
        column.appendChild(card);
        /// Nodo Card-image
        let cardImage = document.createElement('div');
        cardImage.classList.add('card-image');
        card.appendChild(cardImage);
        //// Nodo Figure
        let figure = document.createElement('figure');
        figure.classList.add('image', 'is4by3');
        cardImage.appendChild(figure);
        //// Nodo img
        let img = document.createElement('img');
        img.setAttribute('src', imagen);
        img.setAttribute('alt', nombre);
        figure.appendChild(img);
        /// Nodo Card Content
        let cardContent = document.createElement('div');
        cardContent.classList.add('card-content');
        card.appendChild(cardContent);
        //// Nodo contenido del card
        let content = document.createElement('div');
        content.classList.add('content');
        cardContent.appendChild(content);
        ///// Noto titulo del content
        let title = document.createElement('p');
        title.classList.add('title', 'is-4');
        title.textContent = nombre;
        content.appendChild(title);
        ///// Nodo del subtitulo del content
        let subtitle = document.createElement('p');
        subtitle.classList.add('subtitle');
        subtitle.textContent = `${precio} €`
        content.appendChild(subtitle);
        ///// Boton añadir al carrito del content
        let botonAnyadirAlCarrito = document.createElement('button');
        botonAnyadirAlCarrito.classList.add('button', 'is-success', 'is-small');
        content.appendChild(botonAnyadirAlCarrito);
        ///// Icono de añadir al carrito
        let iconoAnyadirAlCarrito = document.createElement('i');
        iconoAnyadirAlCarrito.classList.add('fas', 'fa-plus')
        botonAnyadirAlCarrito.appendChild(iconoAnyadirAlCarrito);
        // Agregamos al html
        fichaProducto.appendChild(column);
    }
    /// Loop para generar todas las cartas segun la información
    let loopCreacion = () => {
        let contador = 0;
        for( let producto of PRODUCTOS) {
            contador += 1;
            let inicio = (pag - 1) * MAX_PRODUCTS_PAGE;
            let final = inicio + MAX_PRODUCTS_PAGE;
            if (inicio < contador && contador <= final) {
                addProducto(producto.id, producto.nombre, producto.imagen, producto.precio);
            }
        }
    }

    // Buscar producto
    let buscarProducto = () => {
        // Eliminamos todos los items
        let cardItemns = document.querySelectorAll('#fichaProducto .column');
        for(let item of cardItemns) {
            item.remove();
        }
        // Optenemos la letra
        let resultadoBuscqueda = inputBuscar.value;
        // Buscamos coincidencia en la base de datos
        for(let producto of PRODUCTOS){
            // Contador de productos
            if(producto.nombre.toUpperCase().includes(resultadoBuscqueda.toUpperCase())) {
                // Renderizamos
                addProducto(
                    producto.id,
                    producto.nombre,
                    producto.imagen,
                    producto.preci
                );
            }
        }
    }

    let irPaginaAnterior = () => {
        pag -= 1;
        let cardItemns = document.querySelectorAll('#fichaProducto .column');
        for(let item of cardItemns) {
            item.remove();
        }
        if(pag == 0) {
            pag = 1;
        }
        loopCreacion();
    }

    let irPaginaSiguiente = () => {
        pag += 1;
        let cardItemns = document.querySelectorAll('#fichaProducto .column');
        for(let item of cardItemns) {
            item.remove();
        }
        if (pag == 3){
            pag = 2;
        }
        loopCreacion();
    }

    // Eventos
   loopCreacion();
   inputBuscar.addEventListener('keyup', buscarProducto);
   botonAnterior.addEventListener('click', irPaginaAnterior);
   botonSiguiente.addEventListener('click', irPaginaSiguiente);

    // Inicio
});